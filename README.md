# Commons Reflection
###### Java 1.6+ Support
Reflection Utilities

 
## Class Introspection
This class can be used to extract information about class meta data such as interfaces, methods and field using filters for [classes](#markdown-header-class-filter), [fields](#markdown-header-field-filter), [methods](#markdown-header-method-filter) and [constructors](#markdown-header-constructor-filter).

##### Example:  Getters
```java
 ClassIntrospector introspector = new ClassIntrospectorImpl(MyClass.class);

 Set<Method> getters = introspector.getMethods(new MethodFilterBuilder()
                                    .name("^get[A-Z].*$")
                                    .or().name("^is[A-Z].*$")
                                    .or().name("^has[A-Z].*$")
                                    .isPublic()
                                    .not().isStatic()
                                    .not().returnType(Void.TYPE)
                                    .numberOfParameters(0).build());
```
## Class Filter
ClassFilter is used to filter classes by applying the filter on set of classes.

##### Example: Filter test classes
```java
ClassFilter testClassFilter = new ClassFilterBuilder()
                                .isPublic()
                                .not().isAbstract().or().isInterface()
                                .name("^.*Test$")
                                .build();
```

| Type                        | Builder Method(s)                           | Description                |
| :---------------------------|:--------------------------------------------|:---------------------------|
| Annotated                   | annotated                                   | Classes which are annotated with the supplied annotation class |
| Assignable From             | extendsType                                 | Classes that extends the supplied type |
| Assignable To               | superType                                   | Classes which type is super type of supplied type  |
| Is Annotation               | isAnnotation                                | Classes that is annotation |
| Is Anonymous                | isAnonymous                                 | Classes that is anonymous |
| Is Array                    | isArray                                     | Classes that is array |
| Is Enum                     | isEnum                                      | Classes that is enum       |
| Is Interface                | isInterface                                 | Classes that is interface |
| Is Local                    | isLocal                                     | Classes that is local class     |
| Is Member                   | isMember                                    | Classes that is member class |
| Is Primitive                | isPrimitive                                 | Classes that is primitive|
| Is Synthetic                | isSynthetic                                 | Classes that is synthetic|
| Meta Annotated              | metaAnnotated                               | Classes which have annotation(s) that are annotated with the supplied annotation class |
| Modifier                    | byModifiers, isDefault, isPublic, isPrivate, isProtected, isStatic, isFinal, isAbstract | Classses with a specific modifier      |
| Name                        | name                                        | Classes that matches the supplied regular expression |
| Negation (operator)         | not                                         | NOT Operator       |
| Or (operator)               | or                                          | OR Operator |



## Field Filter
FieldFilter is used to filter fields by applying the filter on a set of fields.

##### Example: Numeric JSON Properties
```java
FieldFilter numericJsonPropertyFilter = new FieldFilterBuilder()
                                .annotated(JsonProperty.class)
                                .not().isStatic().or().isEnumConstant().or().isFinal()
                                .extendsType(Number.class)
                                .build();
```

| Type                        | Builder Method(s)                           | Description                |
| :---------------------------|:--------------------------------------------|:---------------------------|
| Annotated                   | annotated                                   | Fields which are annotated with the supplied annotation class |
| Assignable From             | extendsType                                 | Fields which type extends the supplied type|
| Assignable To               | superType                                   | Fields which type is super type of supplied type  |
| Field Class                 | typeFilter                                  | Fields with type that matches a supplied ClassFilter |
| Is Enum Constant            | isEnumConstant                              | Fields which are enum constants |
| Meta Annotated              | metaAnnotated                               | Fields which have annotation(s) that are annotated with the supplied annotation class |
| Modifier                    | byModifiers, isDefault, isPublic, isPrivate, isProtected, isStatic, isTransient, isFinal, isVolatile | Fields with a specific modifier      |
| Name                        | name                                        | Methods that matches the supplied regular expression |
| Negation (operator)         | not                                         | NOT Operator       |
| Or (operator)               | or                                          | OR Operator |


## Method Filter
MethodFilter is used to filter classes by applying the filter on a set of methods.

##### Example:  Getters
```java
 MethodFilter getterFilter = new MethodFilterBuilder()
                                    .name("^get[A-Z].*$")
                                    .or().name("^is[A-Z].*$")
                                    .or().name("^has[A-Z].*$")
                                    .isPublic()
                                    .not().isStatic()
                                    .not().returnType(Void.TYPE)
                                    .numberOfParameters(0)
                                    .build();
``` 

| Type                        | Builder Method(s)                           | Description                |
| :---------------------------|:--------------------------------------------|:---------------------------|
| Annotated                   | annotated                                   | Methods which are annotated with the supplied annotation class |
| Assignable From             | parameterExtendsType, parametersExtendsType | Methods which parameter type(s) extends the supplied type(s) |
| Assignable To               | parameterSuperType, parametersSuperType     | Methods which parameter type(s) is super type of supplied type(s)  |
| Meta Annotated              | metaAnnotated                               | Methods which have annotation(s) that are annotated with the supplied annotation class |
| Modifier                    | byModifiers, isDefault, isPublic, isPrivate, isProtected, isSynchronized, isStatic | Methods with a specific modifier |
| Name                        | name                                        | Methods that matches the supplied regular expression |
| Negation (operator)         | not                                         | NOT operator       |
| Number of Parameters   Initial content.     | numberOfParameters                          | Methods that matches the number of supplied parameters |
| Or (operator)               | or                                          | OR Operator       |
| Parameter Class             | parameterTypeFilter                         | Methods with parameter types that matches a supplied ClassFilter |
| Return Type Assignable From | returnTypeExtends                           | Methods which return type extends the supplied type       |
| Return Type                 | returnType                                  | Method which return type equals the supplied return type       |
| Signature                   | hasSignature                                | Methods which parameter types equals the supplied list of types |

## Constructor Filter
ConsructorFilter is used to filter constructors by applying the filter on set of constructors.

##### Example: Filter test classes
```java
ConstructorFilter datasourceConstructor = new ConstructorFilterBuilder()
                                .isPublic()
                                .hasSignature(Datasource.class)                        
                                .build();
```

| Type                        | Builder Method(s)                           | Description                |
| :---------------------------|:--------------------------------------------|:---------------------------|
| Annotated                   | annotated                                   | Constructors which are annotated with the supplied annotation class |
| Assignable From             | parameterExtendsType, parametersExtendsType | Constructors which parameter type(s) extends the supplied type(s) |
| Assignable To               | parameterSuperType, parametersSuperType     | Constructors which parameter type(s) is super type of supplied type(s)  |
| Meta Annotated              | metaAnnotated                               | Constructors which have annotation(s) that are annotated with the supplied annotation class |
| Modifier                    | byModifiers, isDefault, isPublic, isPrivate, isProtected | Constructors with a specific modifier |
| Negation (operator)         | not                                         | NOT operator       |
| Number of Parameters        | numberOfParameters                          | Constructors that matches the number of supplied parameters |
| Or (operator)               | or                                          | OR Operator       |
| Parameter Class             | parameterTypeFilter                         | Constructors with parameter types that matches a supplied ClassFilter |
| Signature                   | hasSignature                                | Constructors which parameter types equals the supplied list of types |

## AnnotationBuilder
When writing and testing framework code the need to create annotation instances on the fly becomes apparent. Therefore this project includes an Annotations Builder using CGLIB.

##### Example: new instance of javax.annotation.Resource
```java
@javax.annotation.Resource(name = "qualifiedName")
```
A new instance of this annotation could then be created by using the builder and setting the name attribute as follows: 
```java
javax.annotation.Resource = new AnnotationBuilder(javax.annotation.Resource.class).attr("name", "qualifiedName").build();
```
