package se.hiq.oss.commons.reflection.filter;

/**
 * Filters classes.
 *
 * @author Rikard Wigforss
 **/
public interface ClassFilter {
    boolean apply(Class<?> clazz);

    String describe();
}
