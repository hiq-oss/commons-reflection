package se.hiq.oss.commons.reflection.filter;

import java.lang.reflect.Constructor;

/**
 * Filters Constructors.
 * <p>
 * Implement this interface to create a Constructor filter.
 *
 * @author rikardwi
 **/
public interface ConstructorFilter {
    @SuppressWarnings("rawtypes")
    boolean apply(Constructor constructor);

    String describe();
}
