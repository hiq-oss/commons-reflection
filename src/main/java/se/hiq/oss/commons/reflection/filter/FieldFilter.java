package se.hiq.oss.commons.reflection.filter;

import java.lang.reflect.Field;

/**
 * Field Filter.
 * <p>
 * Implement this interface to create a field filter.
 *
 * @author rikardwi
 **/
public interface FieldFilter {
    boolean apply(Field field);

    String describe();
}
