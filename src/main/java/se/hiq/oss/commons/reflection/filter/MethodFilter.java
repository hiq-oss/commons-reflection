package se.hiq.oss.commons.reflection.filter;

import java.lang.reflect.Method;

/**
 * Method Filter.
 * <p>
 * Implement this interface to create a MethodFilter.
 *
 * @author rikardwi
 **/
public interface MethodFilter {

    boolean apply(Method method);

    String describe();
}
