package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;
import se.hiq.oss.commons.reflection.filter.MethodFilter;

/**
 * Filters constructors which parameter types is assignable from a specific class.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 *
 * @author rikardwi
 **/
public class AssignableFromParameterFilter implements MethodFilter, ConstructorFilter {

    private int paramterIndex;
    private Class<?> assignbleFromClass;
    private Class<?>[] assignbleFrom;

    public AssignableFromParameterFilter(int paramterIndex, final Class<?> assignbleFromClass) {
        this.paramterIndex = paramterIndex;
        this.assignbleFromClass = assignbleFromClass;
    }

    public AssignableFromParameterFilter(final Class<?>... assignbleFrom) {
        this.assignbleFrom = assignbleFrom;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean apply(Constructor constructor) {
        if (assignbleFromClass != null) {
            if ((constructor.getParameterTypes().length - 1) < paramterIndex) {
                return false;
            }
            return assignbleFromClass.isAssignableFrom(constructor.getParameterTypes()[paramterIndex]);
        } else if (assignbleFrom.length == constructor.getParameterTypes().length) {
            for (int i = 0; i < assignbleFrom.length; i++) {
                if (!assignbleFrom[i].isAssignableFrom(constructor.getParameterTypes()[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean apply(Method method) {
        if (assignbleFromClass != null) {
            if ((method.getParameterTypes().length - 1) < paramterIndex) {
                return false;
            }
            return assignbleFromClass.isAssignableFrom(method.getParameterTypes()[paramterIndex]);
        } else if (assignbleFrom.length == method.getParameterTypes().length) {
            for (int i = 0; i < assignbleFrom.length; i++) {
                if (!assignbleFrom[i].isAssignableFrom(method.getParameterTypes()[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public String describe() {
        if (assignbleFromClass != null) {
            return "paremeter at index " + paramterIndex + " extends " + assignbleFromClass.getName();
        } else {
            return "parameters extends" + StringUtils.join(assignbleFrom, ", ");
        }
    }

}
