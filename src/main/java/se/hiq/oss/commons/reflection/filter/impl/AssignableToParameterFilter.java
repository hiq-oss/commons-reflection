package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;
import se.hiq.oss.commons.reflection.filter.MethodFilter;

/**
 * Filters constructors which parameter types is assignable to a specific class.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 * @author rikardwi
 **/
public class AssignableToParameterFilter implements MethodFilter, ConstructorFilter {

    private int paramterIndex;
    private Class<?> assignbleToClass;
    private Class<?>[] assignbleTo;

    public AssignableToParameterFilter(int paramterIndex, final Class<?> assignbleToClass) {
        this.paramterIndex = paramterIndex;
        this.assignbleToClass = assignbleToClass;
    }

    public AssignableToParameterFilter(final Class<?>... assignbleFrom) {
        this.assignbleTo = assignbleFrom;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public boolean apply(Constructor constructor) {
        if (assignbleToClass != null) {
            if ((constructor.getParameterTypes().length - 1) < paramterIndex) {
                return false;
            }
            return constructor.getParameterTypes()[paramterIndex].isAssignableFrom(assignbleToClass);
        } else if (assignbleTo.length == constructor.getParameterTypes().length) {
            for (int i = 0; i < assignbleTo.length; i++) {
                if (!constructor.getParameterTypes()[i].isAssignableFrom(assignbleTo[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean apply(Method method) {
        if (assignbleToClass != null) {
            if ((method.getParameterTypes().length - 1) < paramterIndex) {
                return false;
            }
            return method.getParameterTypes()[paramterIndex].isAssignableFrom(assignbleToClass);
        } else if (assignbleTo.length == method.getParameterTypes().length) {
            for (int i = 0; i < assignbleTo.length; i++) {
                if (!method.getParameterTypes()[i].isAssignableFrom(assignbleTo[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    @Override
    public String describe() {
        if (assignbleToClass != null) {
            return "paremeter at index" + paramterIndex + " is super class of  " + assignbleToClass.getName();
        } else {
            return "parameters is super classes of" + StringUtils.join(assignbleTo, ", ");
        }
    }

}
