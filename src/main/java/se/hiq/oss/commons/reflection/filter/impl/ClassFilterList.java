package se.hiq.oss.commons.reflection.filter.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.ClassFilter;

/**
 * Composite filter that runs the filter list supplied in the constructor.
 * <p>
 * Use this class to combine filters.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 * @author rikard
 **/
public class ClassFilterList implements ClassFilter {

    private ClassFilter[] filters;

    public ClassFilterList(final ClassFilter... filters) {
        this.filters = filters;
    }

    @Override
    public boolean apply(Class<?> clazz) {
        for (ClassFilter filter : filters) {
            if (!filter.apply(clazz)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String describe() {
        List<String> filterDescription = new ArrayList<String>();
        for (ClassFilter filter : filters) {
            filterDescription.add(filter.describe());
        }
        return StringUtils.join(filterDescription.toArray(), " and ");
    }
}
