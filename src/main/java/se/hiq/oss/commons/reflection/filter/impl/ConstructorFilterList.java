package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;


/**
 * List of constructor Filters.
 * <p>
 * Evaluates the filters in an AND manner.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 * @author rikardwi
 **/
public class ConstructorFilterList implements ConstructorFilter {
    private ConstructorFilter[] filters;

    public ConstructorFilterList(final ConstructorFilter... filters) {
        this.filters = filters;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean apply(Constructor constructor) {
        for (ConstructorFilter filter : filters) {
            if (!filter.apply(constructor)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String describe() {
        List<String> filterDescription = new ArrayList<String>();
        for (ConstructorFilter filter : filters) {
            filterDescription.add(filter.describe());
        }
        return StringUtils.join(filterDescription.toArray(), " and ");
    }
}
