package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.FieldFilter;


/**
 * List of Field Filters.
 * <p>
 * Evaluates the filters in an AND manner.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 *
 * @author rikardwi
 **/
public class FieldFilterList implements FieldFilter {

    private FieldFilter[] filters;

    public FieldFilterList(final FieldFilter... filters) {
        this.filters = filters;
    }


    @Override
    public boolean apply(Field field) {
        for (FieldFilter filter : filters) {
            if (!filter.apply(field)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String describe() {
        List<String> filterDescription = new ArrayList<String>();
        for (FieldFilter filter : filters) {
            filterDescription.add(filter.describe());
        }
        return StringUtils.join(filterDescription.toArray(), " and ");
    }
}
