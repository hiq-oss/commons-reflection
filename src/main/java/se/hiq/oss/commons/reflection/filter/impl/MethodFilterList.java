package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import se.hiq.oss.commons.reflection.filter.MethodFilter;


/**
 * List of Method Filters.
 * <p>
 * Evaluates the filters in an AND manner.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 *
 * @author rikardwi
 **/
public class MethodFilterList implements MethodFilter {
    private MethodFilter[] filters;

    public MethodFilterList(final MethodFilter... filters) {
        this.filters = filters;
    }

    @Override
    public boolean apply(Method method) {
        for (MethodFilter filter : filters) {
            if (!filter.apply(method)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String describe() {
        List<String> filterDescription = new ArrayList<String>();
        for (MethodFilter filter : filters) {
            filterDescription.add(filter.describe());
        }
        return StringUtils.join(filterDescription.toArray(), " and ");
    }
}
