package se.hiq.oss.commons.reflection.filter.impl;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import se.hiq.oss.commons.reflection.Modifier;
import se.hiq.oss.commons.reflection.filter.ClassFilter;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;
import se.hiq.oss.commons.reflection.filter.FieldFilter;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.util.ModifierUtils;

/**
 * Filters classes by modifiers.
 * <p>
 * Note: It's not recommended to use this class directly, instead use builders from the se.hiq.oss.commons.reflection.filter.builder package
 *
 * @author rikardwi
 **/
public class ModifierFilter implements ClassFilter, MethodFilter, FieldFilter, ConstructorFilter {

    private int modifier;

    public ModifierFilter(int modifier) {
        this.modifier = modifier;
    }

    public ModifierFilter(final Modifier... modifiers) {
        int modifierInt = 0;
        for (Modifier modifierEnum : modifiers) {
            modifierInt = modifier | modifierEnum.getValue();
        }
        this.modifier = modifierInt;
    }

    @Override
    public boolean apply(Class<?> clazz) {
        return (clazz.getModifiers() & modifier) > 0;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean apply(Constructor constructor) {
        return (constructor.getModifiers() & modifier) > 0;

    }

    @Override
    public boolean apply(Field field) {
        return (field.getModifiers() & modifier) > 0;
    }

    @Override
    public boolean apply(Method method) {
        return (method.getModifiers() & modifier) > 0;
    }

    @Override
    public String describe() {
        return "is " + ModifierUtils.toString(modifier);
    }

}
