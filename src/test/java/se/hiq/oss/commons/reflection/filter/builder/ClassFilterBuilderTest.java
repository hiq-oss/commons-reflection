package se.hiq.oss.commons.reflection.filter.builder;

import java.lang.annotation.Retention;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.commons.reflection.filter.ClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.AnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableFromTypeFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableToTypeFilter;
import se.hiq.oss.commons.reflection.filter.impl.ClassFilterList;
import se.hiq.oss.commons.reflection.filter.impl.IsAnnotationClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsAnonymousClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsArrayClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsEnumClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsInterfaceClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsLocalClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsMemberClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsPrimitiveClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.IsSyntheticClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.MetaAnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.ModifierFilter;
import se.hiq.oss.commons.reflection.filter.impl.NameFilter;
import se.hiq.oss.commons.reflection.filter.impl.NegationClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.OrClassFilter;


@RunWith(MockitoJUnitRunner.class)
public class ClassFilterBuilderTest {


    private ClassFilterBuilder builder = new ClassFilterBuilder();

    @Test(expected = IllegalStateException.class)
    public void buildNoFilter() {
        builder.build();
    }

    @Test
    public void nameTest() {
        ClassFilter filter = builder.name("testName").build();
        assertThat(filter instanceof NameFilter, is(true));
    }

    @Test
    public void isPublicTest() {
        ClassFilter filter = builder.isPublic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isStaticTest() {
        ClassFilter filter = builder.isStatic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isFinalTest() {
        ClassFilter filter = builder.isFinal().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isAbstractTest() {
        ClassFilter filter = builder.isAbstract().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void notFilterTest() {
        ClassFilter filter = builder.not().isPublic().build();
        assertThat(filter instanceof NegationClassFilter, is(true));
    }

    @Test
    public void orFilterTest() {
        ClassFilter filter = builder.isPrivate().or().isProtected().build();
        assertThat(filter instanceof OrClassFilter, is(true));
    }

    @Test
    public void isInterface() {
        ClassFilter filter = builder.isInterface().build();
        assertThat(filter instanceof IsInterfaceClassFilter, is(true));
    }

    @Test
    public void isAnnotation() {
        ClassFilter filter = builder.isAnnotation().build();
        assertThat(filter instanceof IsAnnotationClassFilter, is(true));
    }

    @Test
    public void isAnonymous() {
        ClassFilter filter = builder.isAnonymous().build();
        assertThat(filter instanceof IsAnonymousClassFilter, is(true));
    }

    @Test
    public void isArray() {
        ClassFilter filter = builder.isArray().build();
        assertThat(filter instanceof IsArrayClassFilter, is(true));
    }

    @Test
    public void isEnum() {
        ClassFilter filter = builder.isEnum().build();
        assertThat(filter instanceof IsEnumClassFilter, is(true));
    }


    @Test
    public void isLocal() {
        ClassFilter filter = builder.isLocal().build();
        assertThat(filter instanceof IsLocalClassFilter, is(true));
    }

    @Test
    public void isMember() {
        ClassFilter filter = builder.isMember().build();
        assertThat(filter instanceof IsMemberClassFilter, is(true));
    }

    @Test
    public void isPrimitive() {
        ClassFilter filter = builder.isPrimitive().build();
        assertThat(filter instanceof IsPrimitiveClassFilter, is(true));
    }

    @Test
    public void isSynthetic() {
        ClassFilter filter = builder.isSynthetic().build();
        assertThat(filter instanceof IsSyntheticClassFilter, is(true));
    }

    @Test
    public void extendsType() {
        ClassFilter filter = builder.extendsType(List.class).build();
        assertThat(filter instanceof AssignableFromTypeFilter, is(true));
    }

    @Test
    public void superType() {
        ClassFilter filter = builder.superType(List.class).build();
        assertThat(filter instanceof AssignableToTypeFilter, is(true));
    }

    @Test
    public void annotated() {
        ClassFilter filter = builder.annotated(Retention.class).build();
        assertThat(filter instanceof AnnotatedFilter, is(true));
    }

    @Test
    public void metaAnnotated() {
        ClassFilter filter = builder.metaAnnotated(Retention.class).build();
        assertThat(filter instanceof MetaAnnotatedFilter, is(true));
    }

    @Test
    public void filterList() {
        ClassFilter filter = builder.isPrivate().isStatic().build();
        assertThat(filter instanceof ClassFilterList, is(true));

    }

    @Test
    public void isDefault() {
        ClassFilter filter = builder.isDefault().build();
        assertThat(filter instanceof NegationClassFilter, is(true));
    }

    @Test
    public void byModifiers() {
        ClassFilter filter = builder.byModifiers(Modifier.ABSTRACT).build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }
}
