package se.hiq.oss.commons.reflection.filter.builder;

import java.lang.annotation.Retention;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;
import se.hiq.oss.commons.reflection.filter.impl.AnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableFromParameterFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableToParameterFilter;
import se.hiq.oss.commons.reflection.filter.impl.MetaAnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.ModifierFilter;
import se.hiq.oss.commons.reflection.filter.impl.ParameterClassFilterFilter;
import se.hiq.oss.commons.reflection.filter.impl.ConstructorFilterList;
import se.hiq.oss.commons.reflection.filter.impl.NameFilter;
import se.hiq.oss.commons.reflection.filter.impl.NegationConstructorFilter;
import se.hiq.oss.commons.reflection.filter.impl.NumberOfParametersFilter;
import se.hiq.oss.commons.reflection.filter.impl.OrConstructorFilter;
import se.hiq.oss.commons.reflection.filter.impl.SignatureFilter;


@RunWith(MockitoJUnitRunner.class)
public class ConstructorFilterBuilderTest {


    private ConstructorFilterBuilder builder = new ConstructorFilterBuilder();

    @Test(expected = IllegalStateException.class)
    public void buildEmptyFilter() {
        builder.build();
    }


    @Test
    public void notTest() {
        ConstructorFilter filter = builder.not().isPublic().build();
        assertThat(filter instanceof NegationConstructorFilter, is(true));
    }

    @Test
    public void orTest() {
        ConstructorFilter filter = builder.isPublic().or().isPrivate().or().isProtected().build();
        assertThat(filter instanceof OrConstructorFilter, is(true));
    }

    @Test
    public void listTest() {
        ConstructorFilter filter = builder.isPublic().isPrivate().build();
        assertThat(filter instanceof ConstructorFilterList, is(true));
    }

    @Test
    public void isPublic() {
        ConstructorFilter filter = builder.isPublic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isProtected() {
        ConstructorFilter filter = builder.isProtected().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isPrivate() {
        ConstructorFilter filter = builder.isPrivate().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }


    @Test
    public void isDefault() {
        ConstructorFilter filter = builder.isDefault().build();
        assertThat(filter instanceof NegationConstructorFilter, is(true));
    }


    @Test
    public void annotated() {
        ConstructorFilter filter = builder.annotated(Retention.class).build();
        assertThat(filter instanceof AnnotatedFilter, is(true));
    }

    @Test
    public void metaAnnotated() {
        ConstructorFilter filter = builder.metaAnnotated(Retention.class).build();
        assertThat(filter instanceof MetaAnnotatedFilter, is(true));
    }

    @Test
    public void hasSignature() {
        ConstructorFilter filter = builder.hasSignature(int.class).build();
        assertThat(filter instanceof SignatureFilter, is(true));
    }


    @Test
    public void numberOfParameters() {
        ConstructorFilter filter = builder.numberOfParameters(4).build();
        assertThat(filter instanceof NumberOfParametersFilter, is(true));
    }

    @Test
    public void parametersExtendsType() {
        ConstructorFilter filter = builder.parametersExtendsType(List.class).build();
        assertThat(filter instanceof AssignableFromParameterFilter, is(true));
    }

    @Test
    public void parametersSuperType() {
        ConstructorFilter filter = builder.parametersSuperType(List.class).build();
        assertThat(filter instanceof AssignableToParameterFilter, is(true));
    }

    @Test
    public void parameterExtendsType() {
        ConstructorFilter filter = builder.parameterExtendsType(0, List.class).build();
        assertThat(filter instanceof AssignableFromParameterFilter, is(true));
    }

    @Test
    public void parameterSuperType() {
        ConstructorFilter filter = builder.parameterSuperType(0, List.class).build();
        assertThat(filter instanceof AssignableToParameterFilter, is(true));
    }


    @Test
    public void parameterTypeFilter() {
        ConstructorFilter filter = builder.parameterTypeFilter(0, new NameFilter("Abstract.*")).build();
        assertThat(filter instanceof ParameterClassFilterFilter, is(true));
    }

    @Test
    public void parameterTypesFilter() {
        ConstructorFilter filter = builder.parametersTypesFilter(new NameFilter("Abstract.*")).build();
        assertThat(filter instanceof ParameterClassFilterFilter, is(true));
    }

}
