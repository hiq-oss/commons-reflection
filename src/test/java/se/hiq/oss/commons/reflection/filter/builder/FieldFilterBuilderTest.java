package se.hiq.oss.commons.reflection.filter.builder;

import java.lang.annotation.Retention;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.commons.reflection.filter.FieldFilter;
import se.hiq.oss.commons.reflection.filter.impl.AnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableFromTypeFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableToTypeFilter;
import se.hiq.oss.commons.reflection.filter.impl.MetaAnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.ModifierFilter;
import se.hiq.oss.commons.reflection.filter.impl.TypeClassFilter;
import se.hiq.oss.commons.reflection.filter.impl.FieldFilterList;
import se.hiq.oss.commons.reflection.filter.impl.IsEnumConstantFieldFilter;
import se.hiq.oss.commons.reflection.filter.impl.NameFilter;
import se.hiq.oss.commons.reflection.filter.impl.NegationFieldFilter;
import se.hiq.oss.commons.reflection.filter.impl.OrFieldFilter;


@RunWith(MockitoJUnitRunner.class)
public class FieldFilterBuilderTest {


    private FieldFilterBuilder builder = new FieldFilterBuilder();

    @Test(expected = IllegalStateException.class)
    public void buildEmptyFilter() {
        builder.build();
    }

    @Test
    public void extendsType() {
        FieldFilter filter = builder.extendsType(List.class).build();
        assertThat(filter instanceof AssignableFromTypeFilter, is(true));
    }

    @Test
    public void superType() {
        FieldFilter filter = builder.superType(List.class).build();
        assertThat(filter instanceof AssignableToTypeFilter, is(true));
    }

    @Test
    public void typeFilter() {
        FieldFilter filter = builder.typeFilter(new NameFilter("Abstract*.")).build();
        assertThat(filter instanceof TypeClassFilter, is(true));
    }

    @Test
    public void isEnumConstant() {
        FieldFilter filter = builder.isEnumConstant().build();
        assertThat(filter instanceof IsEnumConstantFieldFilter, is(true));
    }

    @Test
    public void name() {
        FieldFilter filter = builder.name(".*List.*").build();
        assertThat(filter instanceof NameFilter, is(true));
    }

    @Test
    public void annotated() {
        FieldFilter filter = builder.annotated(Retention.class).build();
        assertThat(filter instanceof AnnotatedFilter, is(true));
    }

    @Test
    public void metaAnnotated() {
        FieldFilter filter = builder.metaAnnotated(Retention.class).build();
        assertThat(filter instanceof MetaAnnotatedFilter, is(true));
    }

    @Test
    public void notTest() {
        FieldFilter filter = builder.not().isPublic().build();
        assertThat(filter instanceof NegationFieldFilter, is(true));
    }

    @Test
    public void orTest() {
        FieldFilter filter = builder.isPrivate().or().isProtected().build();
        assertThat(filter instanceof OrFieldFilter, is(true));
    }

    @Test
    public void isPublic() {
        FieldFilter filter = builder.isPublic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isProtected() {
        FieldFilter filter = builder.isProtected().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isPrivate() {
        FieldFilter filter = builder.isPrivate().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isStatic() {
        FieldFilter filter = builder.isStatic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isTransient() {
        FieldFilter filter = builder.isTransient().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isFinal() {
        FieldFilter filter = builder.isFinal().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isVolatile() {
        FieldFilter filter = builder.isVolatile().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isDefault() {
        FieldFilter filter = builder.isDefault().build();
        assertThat(filter instanceof NegationFieldFilter, is(true));
    }

    @Test
    public void byModifiers() {
        FieldFilter filter = builder.byModifiers(Modifier.PROTECTED & Modifier.ABSTRACT).build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void listTest() {
        FieldFilter filter = builder.isStatic().isFinal().build();
        assertThat(filter instanceof FieldFilterList, is(true));
    }

}
