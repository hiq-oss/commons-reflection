package se.hiq.oss.commons.reflection.filter.builder;

import java.lang.annotation.Retention;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.impl.AnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableFromParameterFilter;
import se.hiq.oss.commons.reflection.filter.impl.AssignableToParameterFilter;
import se.hiq.oss.commons.reflection.filter.impl.MetaAnnotatedFilter;
import se.hiq.oss.commons.reflection.filter.impl.MethodFilterList;
import se.hiq.oss.commons.reflection.filter.impl.ModifierFilter;
import se.hiq.oss.commons.reflection.filter.impl.NameFilter;
import se.hiq.oss.commons.reflection.filter.impl.NegationMethodFilter;
import se.hiq.oss.commons.reflection.filter.impl.NumberOfParametersFilter;
import se.hiq.oss.commons.reflection.filter.impl.OrMethodFilter;
import se.hiq.oss.commons.reflection.filter.impl.ParameterClassFilterFilter;
import se.hiq.oss.commons.reflection.filter.impl.ReturnTypeAssignableFromMethodFilter;
import se.hiq.oss.commons.reflection.filter.impl.ReturnTypeMethodFilter;
import se.hiq.oss.commons.reflection.filter.impl.SignatureFilter;



@RunWith(MockitoJUnitRunner.class)
public class MethodFilterBuilderTest {

    private MethodFilterBuilder builder = new MethodFilterBuilder();

    @Test(expected = IllegalStateException.class)
    public void buildEmptyFilter() {
        builder.build();
    }


    @Test
    public void notTest() {
        MethodFilter filter = builder.not().isPublic().build();
        assertThat(filter instanceof NegationMethodFilter, is(true));
    }

    @Test
    public void orTest() {
        MethodFilter filter = builder.isPublic().or().isPrivate().or().isProtected().build();
        System.out.println(filter.describe());
        assertThat(filter instanceof OrMethodFilter, is(true));
    }

    @Test
    public void listTest() {
        MethodFilter filter = builder.isPublic().isSynchronized().build();
        assertThat(filter instanceof MethodFilterList, is(true));
    }

    @Test
    public void isPublic() {
        MethodFilter filter = builder.isPublic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isProtected() {
        MethodFilter filter = builder.isProtected().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isPrivate() {
        MethodFilter filter = builder.isPrivate().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isStatic() {
        MethodFilter filter = builder.isStatic().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void isSynchronized() {
        MethodFilter filter = builder.isSynchronized().build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }


    @Test
    public void isDefault() {
        MethodFilter filter = builder.isDefault().build();
        assertThat(filter instanceof NegationMethodFilter, is(true));
    }

    @Test
    public void byModifiers() {
        MethodFilter filter = builder.byModifiers(Modifier.PUBLIC & Modifier.STATIC).build();
        assertThat(filter instanceof ModifierFilter, is(true));
    }

    @Test
    public void name() {
        MethodFilter filter = builder.name("get[A-Z].*").build();
        assertThat(filter instanceof NameFilter, is(true));
    }

    @Test
    public void annotated() {
        MethodFilter filter = builder.annotated(Retention.class).build();
        assertThat(filter instanceof AnnotatedFilter, is(true));
    }

    @Test
    public void metaAnnotated() {
        MethodFilter filter = builder.metaAnnotated(Retention.class).build();
        assertThat(filter instanceof MetaAnnotatedFilter, is(true));
    }

    @Test
    public void hasSignature() {
        MethodFilter filter = builder.hasSignature(int.class).build();
        assertThat(filter instanceof SignatureFilter, is(true));
    }

    @Test
    public void hasReturnType() {
        MethodFilter filter = builder.returnType(Void.TYPE).build();
        assertThat(filter instanceof ReturnTypeMethodFilter, is(true));
    }

    @Test
    public void returnTypeExtends() {
        MethodFilter filter = builder.returnTypeExtends(Void.TYPE).build();
        assertThat(filter instanceof ReturnTypeAssignableFromMethodFilter, is(true));
    }

    @Test
    public void numberOfParameters() {
        MethodFilter filter = builder.numberOfParameters(4).build();
        assertThat(filter instanceof NumberOfParametersFilter, is(true));
    }

    @Test
    public void parametersExtendsType() {
        MethodFilter filter = builder.parametersExtendsType(List.class).build();
        assertThat(filter instanceof AssignableFromParameterFilter, is(true));
    }

    @Test
    public void parametersSuperType() {
        MethodFilter filter = builder.parametersSuperType(List.class).build();
        assertThat(filter instanceof AssignableToParameterFilter, is(true));
    }

    @Test
    public void parameterExtendsType() {
        MethodFilter filter = builder.parameterExtendsType(0, List.class).build();
        assertThat(filter instanceof AssignableFromParameterFilter, is(true));
    }

    @Test
    public void parameterSuperType() {
        MethodFilter filter = builder.parameterSuperType(0, List.class).build();
        assertThat(filter instanceof AssignableToParameterFilter, is(true));
    }


    @Test
    public void parameterTypeFilter() {
        MethodFilter filter = builder.parameterTypeFilter(0, new NameFilter("Abstract.*")).build();
        assertThat(filter instanceof ParameterClassFilterFilter, is(true));
    }

    @Test
    public void parameterTypesFilter() {
        MethodFilter filter = builder.parametersTypesFilter(new NameFilter("Abstract.*")).build();
        assertThat(filter instanceof ParameterClassFilterFilter, is(true));
    }

}
