package se.hiq.oss.commons.reflection.filter.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.unitils.inject.util.InjectionUtils;
import se.hiq.oss.commons.reflection.filter.ClassFilter;


@RunWith(MockitoJUnitRunner.class)
public class ClassFilterListTest {

    @Mock
    private ClassFilter classFilter1;

    @Mock
    private ClassFilter classFilter2;

    private ClassFilter[] filters;


    private ClassFilterList filter = new ClassFilterList(filters);

    @Test(expected = NullPointerException.class)
    public void testNullFilter() {
        filter.apply(Integer.class);
    }

    @Test
    public void testEmptyFilter() {
        ClassFilter[] filterArray = {};
        InjectionUtils.injectInto(filterArray, filter, "filters");

        assertThat(filter.apply(Integer.class), is(true));
    }

    @Test
    public void testTwoFilterSuccessFilter() {
        ClassFilter[] filterArray = {classFilter1, classFilter2};
        InjectionUtils.injectInto(filterArray, filter, "filters");
        when(classFilter1.apply(Integer.class)).thenReturn(true);
        when(classFilter2.apply(Integer.class)).thenReturn(true);

        assertThat(filter.apply(Integer.class), is(true));
    }

    @Test
    public void testTwoFilterNonPassFilter() {
        ClassFilter[] filterArray = {classFilter1, classFilter2};
        InjectionUtils.injectInto(filterArray, filter, "filters");
        when(classFilter1.apply(Integer.class)).thenReturn(true);
        when(classFilter2.apply(Integer.class)).thenReturn(false);

        assertThat(filter.apply(Integer.class), is(false));
    }
}
