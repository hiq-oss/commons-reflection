package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.unitils.inject.util.InjectionUtils;
import se.hiq.oss.commons.reflection.filter.ConstructorFilter;

@RunWith(MockitoJUnitRunner.class)
public class ConstructorFilterListTest {

    private ConstructorFilter[] filters;

    @Mock
    private ConstructorFilter methodFilter;

    @Mock
    private ConstructorFilter methodFilter2;

    @InjectMocks
    private ConstructorFilterList filter = new ConstructorFilterList(filters);

    @Test(expected = NullPointerException.class)
    public void nullFilterTest() throws SecurityException, NoSuchMethodException {
        Constructor<?> cons = MyClass.class.getConstructor(null);
        filter.apply(cons);
    }

    @Test
    public void emptpyFilterTest() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new ConstructorFilter[]{}, filter, "filters");
        Constructor<?> cons = MyClass.class.getConstructor(null);
        assertThat(filter.apply(cons), is(true));
    }

    @Test
    public void passTrue() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new ConstructorFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Constructor<?> cons = MyClass.class.getConstructor(null);
        when(methodFilter.apply(cons)).thenReturn(true);
        when(methodFilter2.apply(cons)).thenReturn(true);


        assertThat(filter.apply(cons), is(true));
    }

    @Test
    public void passFqlse() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new ConstructorFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Constructor<?> cons = MyClass.class.getConstructor(null);
        when(methodFilter.apply(cons)).thenReturn(false);


        assertThat(filter.apply(cons), is(false));
    }

    @Test
    public void passFalseSecondFilterFalse() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new ConstructorFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Constructor<?> cons = MyClass.class.getConstructor(null);
        when(methodFilter.apply(cons)).thenReturn(true);
        when(methodFilter2.apply(cons)).thenReturn(false);


        assertThat(filter.apply(cons), is(false));
    }


    private static class MyClass {
        public MyClass() {
        }
    }
}
