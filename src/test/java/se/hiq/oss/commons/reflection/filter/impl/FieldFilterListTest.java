package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Field;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.unitils.inject.util.InjectionUtils;
import se.hiq.oss.commons.reflection.filter.FieldFilter;


@RunWith(MockitoJUnitRunner.class)
public class FieldFilterListTest {

    private FieldFilter[] filters;

    @Mock
    private FieldFilter fieldFilter;

    @Mock
    private FieldFilter fieldFilter2;


    private FieldFilterList filter = new FieldFilterList(filters);

    @Test(expected = NullPointerException.class)
    public void nullFiltersTest() throws SecurityException, NoSuchFieldException {
        Field field = MyClass.class.getField("number");
        assertThat(filter.apply(field), is(false));
    }

    @Test
    public void passTrueEmptyLisst() throws SecurityException, NoSuchFieldException {
        InjectionUtils.injectInto(new FieldFilter[]{}, filter, "filters");

        Field field = MyClass.class.getField("number");
        assertThat(filter.apply(field), is(true));
    }


    @Test
    public void passTrueTwoFiltersFirstTrue() throws SecurityException, NoSuchFieldException {
        InjectionUtils.injectInto(new FieldFilter[]{fieldFilter, fieldFilter2}, filter, "filters");
        Field field = MyClass.class.getField("number");
        when(fieldFilter.apply(field)).thenReturn(true);
        when(fieldFilter2.apply(field)).thenReturn(true);


        assertThat(filter.apply(field), is(true));
    }

    @Test
    public void passFalseTwoFiltersFirstTrue() throws SecurityException, NoSuchFieldException {
        InjectionUtils.injectInto(new FieldFilter[]{fieldFilter, fieldFilter2}, filter, "filters");
        Field field = MyClass.class.getField("number");
        when(fieldFilter.apply(field)).thenReturn(true);
        when(fieldFilter2.apply(field)).thenReturn(false);


        assertThat(filter.apply(field), is(false));
    }


    @Test
    public void passFalse() throws SecurityException, NoSuchFieldException {
        InjectionUtils.injectInto(new FieldFilter[]{fieldFilter, fieldFilter2}, filter, "filters");
        Field field = MyClass.class.getField("number");
        when(fieldFilter.apply(field)).thenReturn(false);


        assertThat(filter.apply(field), is(false));
    }


    private static class MyClass {

        @SuppressWarnings("unused")
        public int number;

    }
}
