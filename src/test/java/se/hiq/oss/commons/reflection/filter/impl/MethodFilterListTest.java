package se.hiq.oss.commons.reflection.filter.impl;

import java.lang.reflect.Method;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.unitils.inject.util.InjectionUtils;
import se.hiq.oss.commons.reflection.filter.MethodFilter;


@RunWith(MockitoJUnitRunner.class)
public class MethodFilterListTest {

    private MethodFilter[] filters;

    @Mock
    private MethodFilter methodFilter;

    @Mock
    private MethodFilter methodFilter2;

    @InjectMocks
    private MethodFilterList filter = new MethodFilterList(filters);

    @Test(expected = NullPointerException.class)
    public void nullFilterTest() throws SecurityException, NoSuchMethodException {
        Method method = MyClass.class.getMethod("noParamters");
        filter.apply(method);
    }

    @Test
    public void emptpyFilterTest() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new MethodFilter[]{}, filter, "filters");
        Method method = MyClass.class.getMethod("noParamters");
        assertThat(filter.apply(method), is(true));
    }

    @Test
    public void passTrue() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new MethodFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Method method = MyClass.class.getMethod("noParamters");
        when(methodFilter.apply(method)).thenReturn(true);
        when(methodFilter2.apply(method)).thenReturn(true);


        assertThat(filter.apply(method), is(true));
    }

    @Test
    public void passFqlse() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new MethodFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Method method = MyClass.class.getMethod("noParamters");
        when(methodFilter.apply(method)).thenReturn(false);


        assertThat(filter.apply(method), is(false));
    }

    @Test
    public void passFalseSecondFilterFalse() throws SecurityException, NoSuchMethodException {
        InjectionUtils.injectInto(new MethodFilter[]{methodFilter, methodFilter2}, filter, "filters");
        Method method = MyClass.class.getMethod("noParamters");
        when(methodFilter.apply(method)).thenReturn(true);
        when(methodFilter2.apply(method)).thenReturn(false);


        assertThat(filter.apply(method), is(false));
    }


    private static class MyClass {
        @SuppressWarnings("unused")
        public void noParamters() {
        }
    }
}
