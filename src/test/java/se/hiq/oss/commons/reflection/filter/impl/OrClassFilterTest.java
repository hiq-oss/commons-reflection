package se.hiq.oss.commons.reflection.filter.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.commons.reflection.filter.ClassFilter;

@RunWith(MockitoJUnitRunner.class)
public class OrClassFilterTest {

    @Mock
    private ClassFilter left;

    @Mock
    private ClassFilter right;


    @InjectMocks
    private OrClassFilter filter = new OrClassFilter(left, right);


    @Test
    public void passTrue1() {
        when(left.apply(Integer.class)).thenReturn(false);
        when(right.apply(Integer.class)).thenReturn(true);

        assertThat(filter.apply(Integer.class), is(true));
    }

    @Test
    public void passTrue2() {
        when(left.apply(Integer.class)).thenReturn(true);

        assertThat(filter.apply(Integer.class), is(true));
    }


    @Test
    public void passFalse() {
        when(left.apply(Integer.class)).thenReturn(false);
        when(right.apply(Integer.class)).thenReturn(false);

        assertThat(filter.apply(Integer.class), is(false));
    }
}
